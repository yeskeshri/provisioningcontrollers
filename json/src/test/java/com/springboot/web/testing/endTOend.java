/*package com.springboot.web.testing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Test;
import junit.framework.Assert;


public class endTOend 
{
	
	@Test
	public void testcase1() throws IOException  //String check
	{
		String a="62895385511398:MSISDN:hutch_aon_3:02012021080810185870843011628402131\r\n"
				+ "62895385511398:MSISDN:hutch_aon_3:02012021080816915082581628408153\r\n"
				+ "62895385511398:MSISDN:hutch_aon_3:02012021080814441149011628408893\r\n"
				+ "62895385511398:MSISDN:hutch_aon_3:02012021080816548181311628409399\r\n"
				+ "62895385511398:MSISDN:hutch_aon_3:02012021080815554635441628409493\r\n"
				+ "62895385511398:MSISDN:hutch_aon_3:02012021080817943082821628409733\r\n"
				+ "62895385511398:MSISDN:hutch_aon_3:02012021080811612892251628411317\r\n"
				+ "62895385511398:MSISDN:hutch_aon_3:02012021080814608297241628414123\r\n"
				+ "62895385511398:MSISDN:hutch_aon_3:02012021080817417843531628414936\r\n"
				+ "62895385511398:MSISDN:hutch_aon_3:02012021080816807179541628415536\r\n"
				+ "62895385511398:MSISDN:hutch_aon_3:02022021080816615847391628419064\r\n"
				+ "62895385511398:MSISDN:hutch_aon_3:02022021080816304272731628419064";
		String b="62895385511398:MSISDN:hutch_aon_3:02012021080810185870843011628402131\r\n"
				+ "62895385511398:MSISDN:hutch_aon_3:02012021080816915082581628408153\r\n"
				+ "62895385511398:MSISDN:hutch_aon_3:02012021080814441149011628408893\r\n"
				+ "62895385511398:MSISDN:hutch_aon_3:02012021080816548181311628409399\r\n"
				+ "62895385511398:MSISDN:hutch_aon_3:02012021080815554635441628409493\r\n"
				+ "62895385511398:MSISDN:hutch_aon_3:02012021080817943082821628409733\r\n"
				+ "62895385511398:MSISDN:hutch_aon_3:02012021080811612892251628411317\r\n"
				+ "62895385511398:MSISDN:hutch_aon_3:02012021080814608297241628414123\r\n"
				+ "62895385511398:MSISDN:hutch_aon_3:02012021080817417843531628414936\r\n"
				+ "62895385511398:MSISDN:hutch_aon_3:02012021080816807179541628415536\r\n"
				+ "62895385511398:MSISDN:hutch_aon_3:02022021080816615847391628419064\r\n"
				+ "62895385511398:MSISDN:hutch_aon_3:02022021080816304272731628419064";
		Assert.assertEquals(a,b);
	}
	
	
	@Test
	public void testcase2()
	{
		read r=new read();
		
		ArrayList<JSONObject> json1=new ArrayList<JSONObject>();
		json1=r.Read();
		ArrayList<JSONObject> json=new ArrayList<JSONObject>();
	    JSONObject obj=new JSONObject();
		try {
	       
	        FileReader fileReader = new FileReader("C:\\Users\\User1\\eclipse-workspace\\jason\\viuclip.json");
	        BufferedReader bufferedReader = new BufferedReader(fileReader);
	        String line = null;

	        while((line = bufferedReader.readLine()) != null)
	        {
	            obj = (JSONObject) new JSONParser().parse(line);
	            json.add(obj);
	          
	        }
	        
	        File file = new File("C:\\Users\\User1\\Documents\\Output.txt");
	        
        for(int i=0;i<json1.size();i++)
        {
        	Assert.assertEquals(json.get(i),json.get(i));
        
        }
		}
        catch(FileNotFoundException ex) {
	    	System.out.println(ex);
	        System.out.println("Unable to open file '" + "viuclip.json" + "'");
	        
	    }
	    catch(IOException ex) {
	    	System.out.println(ex);
	        System.out.println("Error reading file '" + "viuclip.json" + "'");                  
	        
	    } catch (ParseException e) {
	    	System.out.println(e);
	        
	        e.printStackTrace();
	    }
		
	}
	
	@Test
	public void testcase3()  //checking the data written on output file with data present on input file
	{
		read r=new read();
		
		ArrayList<JSONObject> json1=new ArrayList<JSONObject>();
		json1=r.Read();
		ArrayList<JSONObject> json=new ArrayList<JSONObject>();
	    JSONObject obj=new JSONObject();
		try {
	       
	        FileReader fileReader = new FileReader("C:\\Users\\User1\\Documents\\Output.txt");
	        BufferedReader bufferedReader = new BufferedReader(fileReader);
	        String line = null;

	        while((line = bufferedReader.readLine()) != null)
	        {
	            obj = (JSONObject) new JSONParser().parse(line);
	            json.add(obj);
	          
	        }
	        
	        //File file = new File("C:\\Users\\User1\\Documents\\Output.txt");
	        
        for(int i=0;i<json1.size();i++)
        {
        	Assert.assertEquals(json.get(i),json.get(i));
        
        }
		}
        catch(FileNotFoundException ex) {
	    	System.out.println(ex);
	        System.out.println("Unable to open file '" + "viuclip.json" + "'");
	        
	    }
	    catch(IOException ex) {
	    	System.out.println(ex);
	        System.out.println("Error reading file '" + "viuclip.json" + "'");                  
	        
	    } catch (ParseException e) {
	    	System.out.println(e);
	        
	        e.printStackTrace();
	    }
		
	}
	
	@Test
	public void testcase4()   //checking the data written on output file with data present on input file
	{
		ArrayList<JSONObject> json=new ArrayList<JSONObject>();
		read r=new read();
		json=r.Read();
		JSONObject obj=new JSONObject();
	        int present=0;
	        int count=1;
	        String a="";  //"02012021080815554635441628409493";
	        for(int i=0;i<json.size();i++)
	        {
	        	JSONObject o=json.get(i);
	        	JSONObject array=(JSONObject)o.get("extraInfo");
	            String transactionId=(String) array.get("transactionId");
	        //  System.out.println(transactionId);
	        
	        	
	           if(transactionId.equalsIgnoreCase(a))
	           {
	        	   present++;
	           }
	        }
	    Assert.assertEquals(present, count);
	        
	      
	
	}
	
	
}*/
