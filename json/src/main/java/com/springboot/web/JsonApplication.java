package com.springboot.web;

import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

//import com.springboot.web.dao.SaveRepository;

@SpringBootApplication
public class JsonApplication {

	public static void main(String[] args) {
	SpringApplication.run(JsonApplication.class, args);
	//SaveRepository repository=context.getBean(SaveRepository.class);
	}

}
