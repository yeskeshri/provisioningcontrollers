package com.springboot.web.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Failed")
public class Failure {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String identifier;
	private String identifierType;
	private String OfferId;
	private String transactionId;
	private String activationType;
	private boolean shouldCumulateValidity;
	private long cumulationLimit;
	private long maxConsumptionLimit;
	private boolean shouldAllowDuplicateTransactionId;
	
	public Failure() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Failure(int id, String identifier, String identifierType, String offerId, String transactionId,
			String activationType, boolean shouldCumulateValidity, long cumulationLimit, long maxConsumptionLimit,
			boolean shouldAllowDuplicateTransactionId) {
		super();
		this.id = id;
		this.identifier = identifier;
		this.identifierType = identifierType;
		OfferId = offerId;
		this.transactionId = transactionId;
		this.activationType = activationType;
		this.shouldCumulateValidity = shouldCumulateValidity;
		this.cumulationLimit = cumulationLimit;
		this.maxConsumptionLimit = maxConsumptionLimit;
		this.shouldAllowDuplicateTransactionId = shouldAllowDuplicateTransactionId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getIdentifierType() {
		return identifierType;
	}

	public void setIdentifierType(String identifierType) {
		this.identifierType = identifierType;
	}

	public String getOfferId() {
		return OfferId;
	}

	public void setOfferId(String offerId) {
		OfferId = offerId;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getActivationType() {
		return activationType;
	}

	public void setActivationType(String activationType) {
		this.activationType = activationType;
	}

	public boolean isShouldCumulateValidity() {
		return shouldCumulateValidity;
	}

	public void setShouldCumulateValidity(boolean shouldCumulateValidity) {
		this.shouldCumulateValidity = shouldCumulateValidity;
	}

	public long getCumulationLimit() {
		return cumulationLimit;
	}

	public void setCumulationLimit(long cumulationLimit) {
		this.cumulationLimit = cumulationLimit;
	}

	public long getMaxConsumptionLimit() {
		return maxConsumptionLimit;
	}

	public void setMaxConsumptionLimit(long maxConsumptionLimit) {
		this.maxConsumptionLimit = maxConsumptionLimit;
	}

	public boolean isShouldAllowDuplicateTransactionId() {
		return shouldAllowDuplicateTransactionId;
	}

	public void setShouldAllowDuplicateTransactionId(boolean shouldAllowDuplicateTransactionId) {
		this.shouldAllowDuplicateTransactionId = shouldAllowDuplicateTransactionId;
	}
	
	
	
	
	/*private String MSISDN;
	private String OfferId;
	private String transactionId;
	
	
	public Failure(int id, String mSISDN, String offerId, String transactionId) {
		this.id = id;
		MSISDN = mSISDN;
		OfferId = offerId;
		this.transactionId = transactionId;
	}
	public Failure() 
	{
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMSISDN() {
		return MSISDN;
	}
	public void setMSISDN(String mSISDN) {
		MSISDN = mSISDN;
	}
	public String getOfferId() {
		return OfferId;
	}
	public void setOfferId(String offerId) {
		OfferId = offerId;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}*/

}
