package com.springboot.web.Controllers;

import java.util.ArrayList;

import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.web.Read.ReadJson;
import com.springboot.web.entities.CorrectOffer;


@RestController
public class TestControllers {
	
	
	
	@GetMapping("/provisioning")
	@ResponseBody
	public String json()
	{
		System.out.println("Welcome to Viuclip page");
		ReadJson obj=new ReadJson();
		obj.read();
		
		
		return "ProvisioningController";
	}
	
	@GetMapping("/test")
	public CorrectOffer test()
	{
		CorrectOffer offer=new CorrectOffer();
		offer.setId(1);
		offer.setIdentifier("99999999999999");
		offer.setOfferId("1234567890987654432312");
		offer.setTransactionId("1234567890987654322344");
		return offer;
		
	}
}
