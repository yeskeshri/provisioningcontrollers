package com.springboot.web.Read;
import java.io.File;
import java.io.PrintStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import com.springboot.web.entities.CorrectOffer;
import com.springboot.web.entities.Failure;
//import com.springboot.web.services.OfferService;


public class ReadJson 
{
	
	//@Autowired
	//public SaveRepository saveRepository; 
	
	
	static ArrayList<JSONObject> obj2=new ArrayList<JSONObject>();
	//static ArrayList<OfferService> o=new ArrayList<OfferService>();
	String offers="hutch_aon_3";
	
	public void read() 
	{
		
		ArrayList<JSONObject> jsonTrue=new ArrayList<JSONObject>();
		ArrayList<JSONObject> jsonFalse=new ArrayList<JSONObject>();
	    JSONObject obj=new JSONObject();
	   
		
		try {
	       
	        FileReader fileReader = new FileReader("C:\\Users\\User1\\eclipse-workspace\\jason\\viuclip.txt");

	        
	        BufferedReader bufferedReader = new BufferedReader(fileReader);
	        String line = null;

	        while((line = bufferedReader.readLine()) != null) {
	            obj = (JSONObject) new JSONParser().parse(line);
	            String offerId=(String)obj.get("offerId");
	            System.out.println(offerId);
	            if(offerId.compareTo(offers)==0)
	            {
	            	jsonTrue.add(obj);
	            	String identifier=(String)obj.get("identifier");
		            String identifierType=(String)obj.get("identifierType");
		            String offerId1=(String)obj.get("offerId");
	            	JSONObject array=(JSONObject)obj.get("extraInfo");
		            String transactionId=(String) array.get("transactionId");
		            String activationType=(String)obj.get("activationType");
		            boolean shouldCumulateValidity=(boolean)obj.get("shouldCumulateValidity");
		            long cumulationLimit=(long)obj.get("cumulationLimit");
		            long maxConsumptionLimit=(long)obj.get("maxConsumptionLimit");
		            boolean shouldAllowDuplicateTransactionId=(boolean)obj.get("shouldAllowDuplicateTransactionId");
	            	
		            
		            System.out.print(identifier+" "+identifierType +" "+offerId1+" "+ transactionId+" "+activationType +" "+shouldCumulateValidity +" "+ cumulationLimit +" "+maxConsumptionLimit +" "+shouldAllowDuplicateTransactionId);
		            System.out.println();
		            
		            
	            	/*JSONObject array=(JSONObject)obj.get("extraInfo");
		            String transactionId=(String) array.get("transactionId");
		            String msisdn=(String) obj.get("MSISDN");
		            String offerId1=(String)obj.get("offerId");
		            String identifier=(String)obj.get("identifier");
		            String identifierType=(String)obj.get("identifierType");
		            String activationType=(String)obj.get("activationType");
		            boolean shouldCumulateValidity=(boolean)obj.get("shouldCumulateValidity");
		            int cumulationLimit=(int)obj.get("cumulationLimit");
		            int maxConsumptionLimit=(int)obj.get("maxConsumptionLimit");
		            boolean shouldAllowDuplicateTransactionId=(boolean)obj.get("shouldAllowDuplicateTransactionId");*/
		            
		            /*CorrectOffer offer=new CorrectOffer();
		            offer.setMSISDN(msisdn);
		            offer.setOfferId(offerId1);
		            offer.setTransactionId(transactionId);
		            this.saveRepository.save(offer);*/
		            
		            
		            CorrectOffer offer=new CorrectOffer();
		            offer.setIdentifier(identifier);
		            offer.setIdentifierType(identifierType);
		            offer.setOfferId(offerId1);
		            offer.setTransactionId(transactionId);
		            offer.setActivationType(activationType);
		            offer.setShouldCumulateValidity(shouldCumulateValidity);
		            offer.setCumulationLimit(cumulationLimit);
		            offer.setMaxConsumptionLimit(maxConsumptionLimit);
		            offer.setShouldAllowDuplicateTransactionId(shouldAllowDuplicateTransactionId);
		            
		            
		            
		            
		            
		            
	            }
	            else
	            {
	            	jsonFalse.add(obj);
	            	String identifier=(String)obj.get("identifier");
		            String identifierType=(String)obj.get("identifierType");
		            String offerId1=(String)obj.get("offerId");
	            	JSONObject array=(JSONObject)obj.get("extraInfo");
		            String transactionId=(String) array.get("transactionId");
		            String activationType=(String)obj.get("activationType");
		            boolean shouldCumulateValidity=(boolean)obj.get("shouldCumulateValidity");
		            long cumulationLimit=(long)obj.get("cumulationLimit");
		            long maxConsumptionLimit=(long)obj.get("maxConsumptionLimit");
		            boolean shouldAllowDuplicateTransactionId=(boolean)obj.get("shouldAllowDuplicateTransactionId");
		            
		            System.out.print(identifier+" "+identifierType +" "+offerId1+" "+ transactionId+" "+activationType +" "+shouldCumulateValidity +" "+ cumulationLimit +" "+maxConsumptionLimit +" "+shouldAllowDuplicateTransactionId);
		            System.out.println();
		            
		            Failure offer=new Failure();
		            offer.setIdentifier(identifier);
		            offer.setIdentifierType(identifierType);
		            offer.setOfferId(offerId1);
		            offer.setTransactionId(transactionId);
		            offer.setActivationType(activationType);
		            offer.setShouldCumulateValidity(shouldCumulateValidity);
		            offer.setCumulationLimit(cumulationLimit);
		            offer.setMaxConsumptionLimit(maxConsumptionLimit);
		            offer.setShouldAllowDuplicateTransactionId(shouldAllowDuplicateTransactionId);
		            
		           // this.saveRepository.save(offer);
	            }
	            
	        }
	        
	        File file = new File("C:\\Users\\User1\\Documents\\SuccessOutput.txt");
	       
	        PrintStream stream = new PrintStream(file);
	        for(int i=0;i<jsonTrue.size();i++)
	        {
	        	System.setOut(stream);
	        	System.out.println(jsonTrue.get(i));
	        }
	        
	        File nfile=new File("C:\\Users\\User1\\Documents\\FailureOutput.txt");
	        PrintStream b = new PrintStream(nfile);
	        for(int i=0;i<jsonFalse.size();i++)
	        {
	        	System.setOut(b);
	        	System.out.println(jsonFalse.get(i));
	        }
	        
	       
		}
	    
	    catch(FileNotFoundException ex) {
	    	System.out.println(ex);
	        System.out.println("Unable to open file '" + "viuclip.json" + "'");
	        
	    }
	    catch(IOException ex) {
	    	System.out.println(ex);
	        System.out.println("Error reading file '" + "viuclip.json" + "'");                  
	        
	    } catch (ParseException e) {
	    	System.out.println(e);
	        
	        e.printStackTrace();
	    }
		

	}
	public ArrayList<JSONObject> o()
	{
		return obj2;
		
	}
	public static void main(String args[])
	{
		ReadJson obj=new ReadJson();
		obj.read();
		
	}

}
